<?php

namespace App\Models;

use Phalcon\Mvc\Model;

class News extends Model
{
    public function initialize()
    {
        $this->hasOne('id', 'NewsInfo', 'newsId', array('alias' => 'info'));
        $this->hasMany('id', 'NewsInfo', 'newsId', array('alias' => 'allInfo'));
    }

    public function afterFetch()
    {
        $this->dateCreatedStr = strtoupper(date('d M Y', strtotime($this->dateCreated)));
        $this->dateUpdatedStr = strtoupper(date('d M Y', strtotime($this->dateUpdated)));
    }
}
