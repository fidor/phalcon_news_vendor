{% extends "base.volt" %}
{% block content %}
    <form id="delete" action="/business/news/delete" method="post" enctype="multipart/form-data">
        <div class="links_top_nav">
            <div class="blockContent withoutPadding">	
                <h1>NEWS</h1>
                <div class="top-buttons-block">
                    <a href="/business/news/create/{{ locale.code }}" class="createNew button btn btn-success add_btn"><i class="fa fa-plus-square-o fa-white"></i>Create post</a>
                    <a href="javascript:void(0)" class="delete_link delete_from_list button btn btn-default red-text disabled"><i class="fa fa-trash-o red-text"></i>Delete</a>
                </div>
            </div>
        </div>
        <div class="blockContent ptop-none flex-height">
            <!-- <div class="viewList">
                    <p>VIEW</p>
                    <ul>
                            <li class="view_1 active"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Lorem ipsum"></a></li>
                            <li class="view_2"><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Lorem ipsum"></a></li>
                    </ul>
            </div> -->
            <div class="main_inf_client">
                <p><span class="title">POSTS LEFT: <span class="text-posted green-text">{{ credits }}</span></p>
                    {#<div class="btn-group leng offer pull-right" role="group">
                            {% for item in locale.list %}
                                    {% set item = item.getLocale() %}
                                    <a href="/business/news/index/{{ order }}/{% if type == 0 %}1{% else %}0{% endif %}/{{ item.code }}" class="btn button {% if item.id == locale.id %}btn-success{% else %}btn-default{% endif %}">{{ item.code }}</a>
                            {% endfor %}
                    </div> #}
                <div class="right-corner"></div>
                <div class="left-corner"></div>
            </div>
            <div class="box-list-group">
                <div class="list-group table-list client_table text-left open-sans news">
                    <div class="list-group-item head">
                        <div class="td td_1">
                            <div class="boxCheckbox all"><div class="check" id="check_all"><i class="fa fa-square-o disabled_icon"></i><i class="fa fa-check-square-o ACTIVE_icon"></i><input type="checkbox" class="styled" name="check_1"></div></div>
                        </div>
                        <div class="td col-md-10 col-sm-10 col_sort">
                            {% if order == '1' %}
                                <a href="/business/news/index/1/{{ type }}/{{ locale.code }}" class="sort {% if type == '0' %}activeUp{% elseif type == '1' %}activeDown{% endif %}">
                                {% else %}
                                    <a href="/business/news/index/1/0/{{ locale.code }}" class="sort">
                                    {% endif %}
                                    TITLE
                                    <span class="caret"><i class="fa fa-caret-up"></i><i class="fa fa-caret-down"></i></span>
                                </a>
                        </div>
                        <div class="td col-md-2 col-sm-2 col_sort text-center">
                            {% if order == '2' %}
                                <a href="/business/news/index/2/{{ type }}/{{ locale.code }}" class="sort {% if type == '0' %}activeUp{% elseif type == '1' %}activeDown{% endif %}">
                                {% else %}
                                    <a href="/business/news/index/2/0/{{ locale.code }}" class="sort">
                                    {% endif %}
                                    <span>POSTED</span>
                                    <span class="caret"><i class="fa fa-caret-up"></i><i class="fa fa-caret-down"></i></span>
                                </a>
                        </div>
                        <div class="td">&nbsp;</div>
                    </div>

                    {% for newsItem in news %}
                        <a class="list-group-item" href="/business/news/show/{{ newsItem.id }}/{{ locale.code }}">
                            <div class="td check_area">
                                <div class="boxCheckbox"><div class="check"><i class="fa fa-square-o disabled_icon"></i><i class="fa fa-check-square-o ACTIVE_icon"></i><input id="check_{{ newsItem.id }}" type="checkbox" class="actualCheckItem" name="item[]" value="{{ newsItem.id }}"/></div></div>
                            </div>
                            <div class="td">
                                <p class="name new_name">{{ newsItem.title }}</p>
                            </div>
                            <div class="td  text-center">
                                <p class="text">{{ newsItem.dateUpdatedStr }}</p>
                            </div>	
                            <div class="td pl20">
                                <div class="marker_large"><i class="fa fa-angle-right"></i></div>
                            </div>
                        </a>
                    {% endfor %}				
                </div>
            </div>
            <div class="box_paging hide">
                <p>Total</p>
                <div class="box_select">
                    <select class="selectpicker">
                        <option>100 per page</option>
                        <option>50 per page</option>
                        <option>20 per page</option>
                    </select>
                </div>
                <div class="listPager">
                    <ul class="pager">
                        <li class="prev_paging"><a href="#"></a></li>
                        <li>1-100</li>
                        <li class="next_paging"><a href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade remove_modal" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>	
                </div>
                <div class="modal-body">
                    <h3>Select at least one item</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary submit_btn_popup" data-dismiss="modal">OK</button>	
                </div>
            </div>
        </div>
    </div>

    {% include('modals/delete') %}

{% endblock %}
