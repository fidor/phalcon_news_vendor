<?php

namespace InnovationDotHome\Users;

use Supermodule\ControllerBase as SupermoduleBase;
use Phalcon\Mvc\Controller;

abstract class News extends Controller
{
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('news')) {
            $this->dataReturn = false;
        }
    }
}
