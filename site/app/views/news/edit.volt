{% extends "base.volt" %}
{% block content %}
    <form class="form-horizontal clearfix edit_news_form" method="post" action="/business/news/save/{{ newsItem.id }}" enctype="multipart/form-data">
        <input type="hidden" name="localeId" value="{{ locale.id }}" />
        <div class="links_top_nav">
            <div class="blockContent withoutPadding">
                <a class="btn btn-default back_btn cancel-check" href="/business/news/show/{{ newsItem.id }}"><i class="fa fa-angle-left"></i></a>	<!--onclick="window.history.back();"-->				
                <h1 class="inner-h1">EDIT NEWS</h1>
                <div class="top-buttons-block">
                    <a class="button btn btn-default cancel-check" href="/business/news/show/{{ newsItem.id }}"><i class="fa fa-times"></i>CANCEL</a>
                    <button type="submit" class="button btn btn-success">UPDATE POST</button>
                    <a class="delete_link button btn btn-default red-text delete-check" href="/business/news/delete/{{ newsItem.id }}"><i class="fa fa-trash-o red-text"></i>Delete</a>
                </div>				
            </div>
        </div>				
        <div class="blockContent ptop-none">
            <div class="block_client_info">
                <div class="main_inf_client">
                    <p><span class="title">{{ newsItem.dateCreated < newsItem.dateUpdated ? 'EDITED' : 'POSTED' }}</span> <span class="text-posted">{{ newsItem.dateCreatedStr }}</span></p>
                        {#
                                        <div class="btn-group leng offer pull-right" role="group">
                            {% for item in locale.list %}
                                {% set item = item.getLocale() %}
                                <a href="/business/news/edit/{{ newsItem.id }}/{{ item.code }}" class="btn button {% if item.id == locale.id %}btn-success{% else %}btn-default{% endif %}">{{ item.code }}</a>
                            {% endfor %}
                        </div>
                                        #}
                    <div class="right-corner"></div>
                    <div class="left-corner"></div>
                </div>
                <div class="inf_wrap">
                    <div class="block_create_new edit clearfix">
                        <div class="new_right top15">
                            <div class="box_preview_mbile"><img src="/business/img/news_text_img.png" alt=""></div>
                            <!-- <a href="#" class="btn btn-default btn-large preview-button" data-toggle="modal" data-target="#myModal">PREVIEW ON MOBILE</a> -->							
                        </div>	
                        <div class="new_left top15">
                            <div class="box_form">
                                <div class="form-group news_heading_block">
                                    <div class="input-group news_heading_block">
                                        <span class="input-group-addon" id="sizing-addon3">Header</span>
                                        <span id="n_charsLeft" class="charsLeft"></span>
                                        <input type="text" class="form-control large bold nh_iput" placeholder="enter announcement title" value="{% if newsItem.info.title is defined %}{{ newsItem.info.title }}{% endif %}" aria-describedby="sizing-addon3" name="title">
                                    </div>
                                    <span class="iconPopover absolute" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="right"  title="Contact information" data-content="Contact Information is shown in your application section. To provide easy access to your clients about your company details."> </span>
                                </div>
                                <div class="load_img_new">			
                                    <img src="{{ newsItem.imageUrl }}" class="img-responsive" alt="" id="upl_img">									
                                    <div class="buttons_slider bottom">
                                        <a href="#" class="button btn btn-default cleanImgError" id="fakeupload"><i class="fa fa-cloud-upload"></i>CHANGE Image</a>
                                        <a href="#" class="button btn btn-default cleanImgError red-text button_2 delete_link" id="deleteImage"><i class="fa fa-trash-o red-text"></i>delete</a>
                                        <input class="input_file_logo hide" value="here the value" type="file" name="uploadimage" id="upload_image" onchange="previewimage('upload_image', 'upl_img');" />
                                        <input type="hidden" id="hasImage" name="hasImage" value="{{ newsItem.imageUrl }}">
                                        <input type="hidden" id="imageAction" name="imageAction" value="none"/>
                                    </div>
                                </div>
                                <div class="form-group text-field">
                                    <div class="box-field news_text_block">										 
                                        <textarea class="form-control" placeholder="enter news text here" name="text">{% if newsItem.info.text is defined %}{{ newsItem.info.text }}{% endif %}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>					
                </div>
            </div>		
        </div>

        <div class="modal fade remove_modal" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>											
                    </div>
                    <div class="modal-body">
                        <h3>Update your News ?</h3>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary submit_btn_popup" data-dismiss="modal"><i class="fa fa-spinner fa-spin" style="display:none;"></i>Update</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL</button>			
                    </div>
                </div>
            </div>
        </div>
    </form>

    {% include('modals/cancel') %}
    {% include('modals/delete') %}

{% endblock %}

{% block script %}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.nh_iput').limit('60', '#n_charsLeft');
        });
    </script>
{% endblock%}