{% extends "base.volt" %}
{% block content %}
    <div class="links_top_nav">
        <div class="blockContent withoutPadding">
            <a href="/business/news "class="btn btn-default back_btn"><i class="fa fa-angle-left"></i></a>
            <h1 class="inner-h1">NEWS</h1>
            <div class="top-buttons-block">
                <a class="button btn btn-default edit_btn" href="/business/news/edit/{{ newsItem.id }}"><i class="fa fa-pencil-square-o"></i>Edit</a>
                <a class="delete_link button btn btn-default red-text delete-check" href="/business/news/delete/{{ newsItem.id }}"><i class="fa fa-trash-o red-text"></i>Delete</a>
            </div>
        </div>
    </div>
    <div class="blockContent ptop-none">
        <div class="block_client_info">
            <div class="main_inf_client">
                <p><span class="title">{{ newsItem.dateCreated < newsItem.dateUpdated ? 'EDITED' : 'POSTED' }}</span> <span class="text-posted">{{ newsItem.dateCreatedStr }}</span></p>
                    {# 
                    <div class="btn-group leng offer pull-right" role="group">
                            {% for item in locale.list %}
                                    {% set item = item.getLocale() %}
                                    <a href="/business/news/show/{{ newsItem.id }}/{{ item.code }}" class="btn button {% if item.id == locale.id %}btn-success{% else %}btn-default{% endif %}">{{ item.code }}</a>
                            {% endfor %}
                    </div> 
                     #}
                <div class="right-corner"></div>
                <div class="left-corner"></div>
            </div>
            <div class="inf_wrap">
                <div class="block_create_new clearfix">
                    <div class="new_right">
                        <div class="cap"></div>
                        <!--<div class="box_preview_mbile"><img src="/business/img/mobile_img_news.jpg" alt=""></div> -->
                        <!-- <a href="#" class="btn btn-default btn-large preview-button" data-toggle="modal" data-target="#myModal">PREVIEW ON MOBILE</a> -->			
                    </div>
                    <div class="new_left">
                        <div class="box_form choosedForm">
                            <form class="form-horizontal clearfix">
                                <div class="form-group last" grey-border-top>
                                    <div class="input-group with-border">
                                        <span class="input-group-addon" id="sizing-addon1">Header</span>							  
                                        <p class="title_heading">{% if newsItem.info.title is defined %}{{ newsItem.info.title }}{% endif %}</p>
                                    </div>
                                    <span class="iconPopover absolute" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="right"  title="Contact information" data-content="Contact Information is shown in your application section. To provide easy access to your clients about your company details."><i class="fa fa-question-circle"></i> </span>
                                </div>
                                <div class="load_img_new">		
                                    <img  src="{{ newsItem.imageUrl }}" class="lazy img-responsive opas0" alt="">
                                    <!--<div class="loading"><i class="fa fa-spinner fa-pulse"></i></div>-->				
                                </div>
                                <div class="form-group text-field full">
                                    <div class="box-field">					 
                                        <p>{% if newsItem.info.text is defined %}{{ newsItem.info.modifyText }}{% endif %}</p>
                                    </div>
                                </div>						
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </div>

    {% include('modals/delete') %}

    <script type="text/javascript">
        $(function () {
            $(window).load(function () {
                $('img.lazy').addClass('opas1');
            });
        });
    </script>
{% endblock %}
