<?php

namespace InnovationDotHome\News;

use Phalcon\Mvc\Controller;

abstract class NewsBaseController extends Controller
{
    public function onConstruct()
    {

    }

    protected function checkAccess($id)
    {
        if ($id === null) {
            $this->dispatcher->forward(array('controller' => 'error', 'action' => 'show404'));
        } else {
            $news = News::findFirst($id);
            if ($news === false) {
                $this->dispatcher->forward(array('controller' => 'error', 'action' => 'show404'));
            } else if ($news->companyId != $this->companyId) {
                $this->dispatcher->forward(array('controller' => 'error', 'action' => 'show403'));
            } else {
                return true;
            }
        }
    }

    /**
     * Upload or remove image
     *
     * @param Request $request upload request
     * @param string $imageAction Action name
     * @return boolean
     */
    public function uploadPhoto($request, $imageAction)
    {
        if ($imageAction == 'update') {
            $imgData = UploadHelper::uploadImage($request);
        } else if ($imageAction == 'delete') {
            $imgData = UploadHelper::deleteImage($this->imageUrl);
        }
        $this->aspectRatio = ($imgData[0]['aspectRatio']) ? $imgData[0]['aspectRatio'] :  0;
        $this->imageUrl = ($imgData[0]['imageUrl']) ?  $imgData[0]['imageUrl'] : '';
        return true;
    }
}
