{% extends "base.volt" %}
{% block content %}
    <form class="form-horizontal clearfix news_add_form" method="post" action="/business/news/save" enctype="multipart/form-data">
        <input type="hidden" name="localeId" value="{{ locale.id }}" />
        <div class="links_top_nav">
            <div class="blockContent withoutPadding">
                <a class="btn btn-default back_btn cancel-check" href="/business/news"><i class="fa fa-angle-left"></i></a>				
                <h1 class="inner-h1">ADD NEWS</h1>
                <div class="top-buttons-block">
                    <button type="submit" class="button btn btn-success news_post_btn">POST</button>
                    <a class="button btn btn-default cancel-check" href="/business/news"><i class="fa fa-times"></i>CANCEL</a>
                </div>				
            </div>
        </div>

        <div class="blockContent ptop-none flex-height">
            <div class="main_inf_client">
                <p><span class="title">Messages left: <span class="text-posted green-text">{{ data.credits }}</span></p>
                <p><span class="title">Posts to: <span class="text-posted green-text">{{ data.userCount }} CLIENTS</span></p>
                {#
                <div class="btn-group leng offer pull-right" role="group">
                        {% for item in locale.list %}
                                {% set item = item.getLocale() %}
                                <a href="/business/news/create/{{ item.code }}" class="btn button {% if item.id == locale.id %}btn-success{% else %}btn-default{% endif %}">{{ item.code }}</a>
                        {% endfor %}
                </div>
                #}
                <div class="right-corner"></div>
                <div class="left-corner"></div>
            </div>
            <div class="inf_wrap">
                <div class="block_create_new top15 clearfix">
                    <div class="new_right">
                        <div class="box_preview_mbile"><img src="/business/img/news_text_img.png" alt=""></div>
                        <!-- <a href="#" class="btn btn-default btn-large preview-button" data-toggle="modal" data-target="#myModal">PREVIEW ON MOBILE</a> -->
                    </div>	
                    <div class="new_left">
                        <div class="box_form">
                            <div class="form-group news_heading_block">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon3">Header</span>
                                    <span id="n_charsLeft" class="charsLeft"></span>
                                    <input type="text" class="form-control large nh_iput" name="title" placeholder="enter news header here" aria-describedby="sizing-addon3" name="title">
                                </div>
                            </div>
                            <div class="load_img_new">
                                <img class="img-responsive" id="upl_img" alt="" />												
                                <div class="buttons_slider bottom">
                                    <a class="button btn btn-default cleanImgError" id="fakeupload"><i class="fa fa-cloud-upload"></i>Upload Image</a>
                                    <a class="button btn btn-default cleanImgError red-text button_2 delete_link" id="deleteImage"><i class="fa fa-trash red-text"></i>delete</a>
                                    <input class="input_file_logo hide" type="file" name="image" id="upload_image" onchange="previewimage('upload_image', 'upl_img');" />
                                    <input type="hidden" id="hasImage" name="hasImage" value="">
                                    <input type="hidden" id="imageAction" name="imageAction" value="none"/>
                                </div>
                            </div>
                            <div class="form-group text-field">
                                <div class="box-field news_text_block">										 
                                    <textarea class="form-control" placeholder="enter news text here" name="text"></textarea>
                                </div>
                            </div>						
                        </div>
                    </div>
                </div>					
            </div>
        </div>
    </form>

    <div class="modal fade remove_modal" id="modal_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h3>Send new Post ?</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary submit_btn_popup" data-dismiss="modal"><i class="fa fa-spinner fa-spin" style="display:none;"></i>POST</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">CANCEL</button>			
                </div>
            </div>
        </div>
    </div>

    {% include('modals/cancel') %}

{% endblock %}

{% block script %}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.nh_iput').limit('69', '#n_charsLeft');
        });
    </script>
{% endblock %}